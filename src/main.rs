use std::io::{self, BufRead};
use std::path::{Component, Path, PathBuf};

struct NamedDir {
    alias: String,
    dir: PathBuf,
}

impl NamedDir {
    pub fn new(mut alias: String, dir: String) -> Self {
        alias.insert(0, '~');
        let dir = PathBuf::from(dir);
        Self { alias, dir }
    }
}

#[derive(Default)]
struct PromptPath {
    head: String,
    tail: String,
}

impl std::fmt::Display for PromptPath {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.tail.is_empty() {
            write!(f, "%F{{4}}{}%f", self.head.replace('%', "%%"))
        } else {
            write!(
                f,
                "%F{{244}}{}%F{{4}}{}%f",
                self.head.replace('%', "%%"),
                self.tail.replace('%', "%%")
            )
        }
    }
}

fn matched_named_dir(path: &Path) -> Option<NamedDir> {
    let mut named_dirs = vec![(std::env::var("HOME").unwrap(), String::new())];
    for line in io::stdin().lock().lines().flatten() {
        let (alias, dir) = line.split_once('=').expect("Wrong named dir format");
        named_dirs.push((dir.to_string(), alias.to_string()));
    }
    named_dirs
        .into_iter()
        .filter(|(dir, _)| path.starts_with(dir))
        .max_by_key(|(dir, _)| dir.len())
        .map(|(dir, alias)| NamedDir::new(alias, dir))
}

fn wrapped_prompt() -> PromptPath {
    let mut input_dir = PathBuf::from(std::env::args_os().nth(1).unwrap());
    let mut prompt = PromptPath::default();

    if let Some(named_dir) = matched_named_dir(&input_dir) {
        input_dir = input_dir.strip_prefix(named_dir.dir).unwrap().to_path_buf();
        prompt.head.push_str(&named_dir.alias);
    }

    let mut components = input_dir.components().peekable();

    loop {
        let component = components.next();

        match component {
            None => break,
            Some(Component::RootDir) => prompt.head.push('/'),
            Some(Component::Normal(comp)) => {
                if prompt.head.as_bytes().iter().last() != Some(&b'/') {
                    prompt.head.push('/');
                }
                match components.peek() {
                    Some(_) => prompt
                        .head
                        .push_str(&comp.to_string_lossy().chars().next().unwrap().to_string()),
                    None => prompt.tail.push_str(&comp.to_string_lossy()),
                }
            }
            _ => unreachable!(),
        }
    }
    prompt
}

fn main() {
    println!("{}", wrapped_prompt());
}
